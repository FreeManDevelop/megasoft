﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using TestProject.Models;
using TestProject.Models.Home;
using TestProject.Repository;

namespace TestProject.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly GenericRepository<QuestionsModel> _questionsRepository;
        private readonly GenericRepository<AnswersModel> _answersRepository;

        public HomeController()
        {
            _questionsRepository = new GenericRepository<QuestionsModel>();
            _answersRepository = new GenericRepository<AnswersModel>();
        }

        public ActionResult Index()
        {
            ViewBag.Questions = _questionsRepository.Get().OrderBy(c => c.Position);

            return View();
        }

        [HttpPost]
        public ActionResult Index(QuestionsViewModel model)
        {
            if (ModelState.IsValid)
            {
                var answers = new List<AnswersModel>();
                var currenUserId = User.Identity.GetUserId();

                for (int i = 0; i < model.Answers.Length; i++)
                {
                    answers.Add(new AnswersModel
                    {
                        Id = Guid.NewGuid(),
                        Answer = model.Answers[i],
                        QuestionsId = model.QuestionId[i],
                        UserId = currenUserId
                    });
                }

                _answersRepository.AddAndUpdate(answers);
                return PartialView("ThankYou");
            }

            ViewBag.Questions = _questionsRepository.Get().OrderBy(c => c.Position); ;
            return View();
        }
    }
}
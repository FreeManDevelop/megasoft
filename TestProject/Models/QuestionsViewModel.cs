﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestProject.Models
{
    public class QuestionsViewModel
    {
        public string[] Answers { get; set; }
        public Guid[] QuestionId  { get; set; }
    }
}
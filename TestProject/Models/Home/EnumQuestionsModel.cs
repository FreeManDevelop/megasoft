﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestProject.Models.Home
{
    public class EnumQuestionsModel
    {
        public Guid Id { get; set; }
        public string Value { get; set; }

        public Guid QuestionsId { get; set; }
        public QuestionsModel Questions { get; set; }
    }
}
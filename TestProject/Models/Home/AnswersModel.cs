﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestProject.Models.Home
{
    public class AnswersModel
    {
        public Guid Id { get; set; }
        public string Answer { get; set; }

        public Guid QuestionsId { get; set; }
        public QuestionsModel Questions { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}
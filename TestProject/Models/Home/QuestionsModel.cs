﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using TestProject.Enum;

namespace TestProject.Models.Home
{
    public class QuestionsModel
    {
        public Guid Id { get; set; }
        public string Question { get; set; }
        public bool IsEnum { get; set; }
        public int Position { get; set; }

        public TypeOfQuestion TypeOfQuestion { get; set; }
        public virtual ICollection<EnumQuestionsModel> EnumQuestions { get; set; }
    }
}
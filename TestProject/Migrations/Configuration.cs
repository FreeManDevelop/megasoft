﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using TestProject.Enum;
using TestProject.Models;
using TestProject.Models.Home;

namespace TestProject.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            var familyQId = Guid.NewGuid();
            var genderQId = Guid.NewGuid();

            if (context.Questions.Count() == 0)
            {
                context.Questions.AddOrUpdate
                    (
                        new QuestionsModel
                        {
                            Id = Guid.NewGuid(),
                            Question = "Введите имя",
                            TypeOfQuestion = TypeOfQuestion.String,
                            Position = 1
                        },
                        new QuestionsModel
                        {
                            Id = Guid.NewGuid(),
                            Question = "Введите возраст",
                            TypeOfQuestion = TypeOfQuestion.Int,
                            Position = 2
                        },
                        new QuestionsModel
                        {
                            Id = genderQId,
                            Question = "Введите пол",
                            TypeOfQuestion = TypeOfQuestion.String,
                            IsEnum = true,
                            Position = 3
                        },
                        new QuestionsModel
                        {
                            Id = Guid.NewGuid(),
                            Question = "Введите дату",
                            TypeOfQuestion = TypeOfQuestion.DateTime,
                            Position = 4
                        },
                        new QuestionsModel
                        {
                            Id = familyQId,
                            Question = "Введите семейное положение",
                            TypeOfQuestion = TypeOfQuestion.String,
                            IsEnum = true,
                            Position = 5
                        },
                        new QuestionsModel
                        {
                            Id = Guid.NewGuid(),
                            Question = "Любите ли вы программировать",
                            TypeOfQuestion = TypeOfQuestion.Flag,
                            IsEnum = true,
                            Position = 6
                        }
                    );

                context.SaveChanges();
            }

            if (context.Questions.Count() > 0 && context.EnumQuestions.Count() == 0)
            {
                context.EnumQuestions.AddOrUpdate
                    (
                        new EnumQuestionsModel
                        {
                            Id = Guid.NewGuid(),
                            Value = "Мужчина",
                            QuestionsId = genderQId
                        },
                        new EnumQuestionsModel
                        {
                            Id = Guid.NewGuid(),
                            Value = "Женщина",
                            QuestionsId = genderQId
                        },
                        new EnumQuestionsModel
                        {
                            Id = Guid.NewGuid(),
                            Value = "Холост",
                            QuestionsId = familyQId
                        },
                        new EnumQuestionsModel
                        {
                            Id = Guid.NewGuid(),
                            Value = "Женат",
                            QuestionsId = familyQId
                        },
                        new EnumQuestionsModel
                        {
                            Id = Guid.NewGuid(),
                            Value = "Разведен(на)",
                            QuestionsId = familyQId
                        },
                        new EnumQuestionsModel
                        {
                            Id = Guid.NewGuid(),
                            Value = "Вдов(а)",
                            QuestionsId = familyQId
                        }
                    );

                context.SaveChanges();
            }
        }
    }
}
namespace TestProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Position_QuestionsModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuestionsModels", "Position", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.QuestionsModels", "Position");
        }
    }
}

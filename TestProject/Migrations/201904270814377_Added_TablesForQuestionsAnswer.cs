namespace TestProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_TablesForQuestionsAnswer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnswersModels",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Answer = c.String(),
                        QuestionsId = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionsModels", t => t.QuestionsId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.QuestionsId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.QuestionsModels",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Question = c.String(),
                        IsEnum = c.Boolean(nullable: false),
                        TypeOfQuestion = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EnumQuestionsModels",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Value = c.String(),
                        QuestionsId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionsModels", t => t.QuestionsId, cascadeDelete: true)
                .Index(t => t.QuestionsId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnswersModels", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AnswersModels", "QuestionsId", "dbo.QuestionsModels");
            DropForeignKey("dbo.EnumQuestionsModels", "QuestionsId", "dbo.QuestionsModels");
            DropIndex("dbo.EnumQuestionsModels", new[] { "QuestionsId" });
            DropIndex("dbo.AnswersModels", new[] { "UserId" });
            DropIndex("dbo.AnswersModels", new[] { "QuestionsId" });
            DropTable("dbo.EnumQuestionsModels");
            DropTable("dbo.QuestionsModels");
            DropTable("dbo.AnswersModels");
        }
    }
}

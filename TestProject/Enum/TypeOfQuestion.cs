﻿namespace TestProject.Enum
{
    public enum TypeOfQuestion
    {
        String = 1,
        Int,
        DateTime,
        Flag
    }
}